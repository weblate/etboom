import React,{useRef} from "react";

import {useNavigate } from 'react-router-dom';

import {useIntl} from 'react-intl';

import { Button } from 'primereact/button';
import { Menu } from 'primereact/menu';

import {setLocale} from "../services/i18n.js";

import TotemHead from "../images/totem-head.jsx";

const titleStyle = {
    fontFamily: "Coiny",
    letterSpacing: "5px",
    fontSize:"2vw",
    fontVariant: "small-caps",
};

function Header() {

    const onLangChange = (lang) => {
        setLocale(lang).then(()=>window.location.reload(true));
    };

    const navigate = useNavigate();
    const navigateTo = (to) => {
        navigate(to, { replace: true });
    };

    const intl = useIntl();
    
    const menuLang = useRef(null);
    
    const items = [
        {
        label: intl.formatMessage({ id: 'key.lang.french'}), 
        command: () => {onLangChange("fr");}
        },
        {
        label: intl.formatMessage({ id: 'key.lang.english'}), 
        command: () => {onLangChange("en");}
        },
        {
        label: intl.formatMessage({ id: 'key.lang.german'}), 
        command: () => {onLangChange("de");}
        }
    ];

    return (
        <div className="flex flex-columun surface-ground m-1 border-round">
                <div className="flex flex-grow-0 align-items-center">
                    <Button className="p-button-rounded p-button-text p-button-lg" onClick={()=>navigateTo("/")} tooltip={intl.formatMessage({ id: 'key.tooltip.logo'})} >
                        <TotemHead size={'3rem'}/>
                    </Button>
                </div>
                <div className="flex flex-grow-1 m-3 align-items-center justify-content-center ">
                    <span className="hidden md:flex text-4xl font-bold text-primary" style={titleStyle}>Et Boom</span>
                </div>
                <div className="flex flex-grow-0 align-items-center" >
                    <Menu model={items} popup ref={menuLang} id="popup_menu" />
                    <Button icon="pi pi-globe" className="p-button-rounded p-button-text p-button-lg"  onClick={(event) => menuLang.current.toggle(event)} aria-controls="popup_menu" aria-haspopup />
                </div>
        </div>
    );
}

export default Header