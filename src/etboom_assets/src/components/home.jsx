import React,{useState,useRef, Fragment} from "react";

import {useNavigate } from 'react-router-dom';

import { Button } from 'primereact/button';
import { Sidebar } from 'primereact/sidebar';
import TerminalPanel  from "./terminal-panel";

import Calendar from '../images/calendar';
import Gecko from "../images/gecko";
import DreamCatcher from "../images/dream-catcher";

import {useIntl} from 'react-intl';

import '../styles/home.css'

function Home() {
    const [visibleTerminal, setVisibleTerminal] = useState(false);
    const intl = useIntl();

    const navigate = useNavigate();
    const navigateTo = (to) => {
        navigate(to, { replace: true });
    };

    return (
        <Fragment>
            <div className="flex flex-row min-w-full">
                <div className="flex flex-grow-0 justify-content-center align-items-center w-2 m-1">

                </div>
                <div className="flex flex-grow-1 justify-content-center align-items-center m-1">
                    <div className="flex flex-row align-items-center m-1 p-1">
                        {/*<Calendar textColor="blue" polyColor="red"/>*/}
                        <div className="flex flex-grow-0 m-1 menu-i">
                            <Button className="p-button-rounded p-button-text" onClick={()=>navigateTo("/date_pool")} tooltip={intl.formatMessage({ id: 'key.tooltip.menu.date'})} tooltipOptions={{position: 'top'}}>
                                <Calendar size={'10vw'}/>
                            </Button>
                        </div>
                        <div className="flex flex-grow-0 m-1">
                            <Button className="p-button-rounded p-button-text" tooltip={intl.formatMessage({ id: 'key.tooltip.menu.sondage'})} tooltipOptions={{position: 'top'}}>
                                <DreamCatcher size={'10vw'}/>
                            </Button>
                        </div>
                        <div className="flex flex-grow-0 m-1">
                            <Button className="p-button-rounded p-button-text" onClick={() => setVisibleTerminal(true)} tooltip={intl.formatMessage({ id: 'key.tooltip.menu.cli'})} tooltipOptions={{position: 'top'}}>
                                <Gecko size={'10vw'}/>
                            </Button>
                        </div>
                    </div>
                </div>
                <div className="flex flex-grow-0 justify-content-center align-items-center w-2 m-1">
                

                </div>
                
            </div>
            <Sidebar className="p-sidebar-lg" visible={visibleTerminal} position="right"  onHide={() => setVisibleTerminal(false)}>
                <TerminalPanel/>
            </Sidebar>
        </Fragment>
        )
}

export default Home