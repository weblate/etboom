import React from "react";

import {useNavigate } from 'react-router-dom';

import { Button } from 'primereact/button';
import {useIntl} from 'react-intl';

import MenuThemeSelector from '../themes/button-menu-theme';

import Gnu from "../images/gnu";
import GitLab from "../images/gitlab";

function Footer() {

    const intl = useIntl();

    const navigate = useNavigate();
    const navigateTo = (to) => {
        navigate(to, { replace: true });
    };
    
    return (
        <div className="flex flex-columun surface-ground m-1 border-round">
                <div className="flex flex-grow-0 align-items-center">
                    <Button className="p-button-rounded p-button-text" onClick={()=>navigateTo("/license")} tooltip={intl.formatMessage({ id: 'key.license.agpl'})} >
                        <Gnu size={'1.4rem'} />
                    </Button>
                </div>
                <div className="flex flex-grow-0 align-items-center">
                    <Button className="p-button-rounded p-button-text" onClick={(e)=>{ e.preventDefault();window.open(window.appinfo.source,'_blank');}} tooltip={intl.formatMessage({ id: 'key.source'})}>
                        <GitLab size={'1.4rem'} />
                    </Button>
                </div>
                <div className="flex flex-grow-1 align-items-center justify-content-center">
                    <span className="text-xs">{window.appinfo ? "Version: " + window.appinfo.version : ""}</span>
                </div>
                <div className="flex flex-grow-0 align-items-center">
                    <MenuThemeSelector/>
                </div>   
        </div>
    );
}

export default Footer