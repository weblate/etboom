import React, { useEffect,useState, Suspense} from 'react';
import { IntlProvider } from "react-intl";
import Home from "./components/home";
import {getDefaultLocale,getLocale,getMessage} from "./services/i18n.js";

import { HashRouter as Router, Route, Routes } from 'react-router-dom';

import ThemeSelector from './themes/theme_selector';
const GnuAffero = React.lazy(() => import ('./components/gnu_affero'));
import Header from './components/header';
import Footer from './components/footer';
import Entry from './forms/date_pool/entry';

import PrimeReact from 'primereact/api';

import { ProgressSpinner } from 'primereact/progressspinner';

import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';
import 'primeflex/primeflex.css';
import './styles/app.css'

// Ripple is an optional animation for the supported components such as buttons.
PrimeReact.ripple = true;

// Input fields come in two styles, default is outlined with borders around the field whereas filled
PrimeReact.inputStyle = 'filled';

import "@fontsource/coiny";
import "@fontsource/poppins";

function App() {
  const [locale,setLocale] = useState(getLocale());
  const [messages,setMessages] = useState(undefined);

  useEffect(() => {
    console.info('mount App');
    getMessage().then((m)=>setMessages(m));
  }, [])

  const fallback = <div className="flex justify-content-start align-items-center min-h-screen"><ProgressSpinner/></div>;

  const Display = () => {
    if (messages!=undefined) {
      return (
        <IntlProvider messages={messages} locale={locale} defaultLocale={getDefaultLocale()}>
          <ThemeSelector>
            <div className="flex flex-column min-h-screen bg-primary-reverse">
              <Router>
                <Header/>
                <div className="flex flex-grow-1">
                    <Routes>
                      <Route exact path="/" element={<Home/>}/>
                      <Route exact path="/date_pool" element={<Entry/>}/>
                      <Route exact path="/license" element={
                        <Suspense fallback={fallback}>
                          <GnuAffero/>
                        </Suspense>
                      }/>
                    </Routes>
                </div>
                <Footer />
              </Router> 
            </div>   
          </ThemeSelector>
        </IntlProvider>
      );
    } else {
      return fallback
    }
  }

  return <Display/>
}

export default App;