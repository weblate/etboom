import * as React from "react"

const GitLab = (props) => (
    <svg version="1.1" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg" width={props.size} height={props.size} {...props}>
        <g>
            <g transform="matrix(2.6523 0 0 2.6523 -409.43 -416.95)">
                <circle 
                    cx="251.55"
                    cy="254.24" r="92.24"
                    style={{
                        fillRule: "evenodd",
                        fill: "var(--primary-color)",
                        strokeOpacity: 0
                    }}/>
                <g transform="translate(190.11 197.65)">
                    <path className="st0" d="m122.65 64.72-6.89-21.15-13.61-41.96c-0.7-2.15-3.74-2.15-4.47 0l-13.62 41.93h-45.25l-13.62-41.93c-0.7-2.15-3.74-2.15-4.47 0l-13.59 41.93-6.89 21.18c-0.62 1.92 0.05 4.04 1.7 5.24l59.5 43.23 59.5-43.23c1.64-1.19 2.34-3.31 1.71-5.24z"/>
                    <polygon className="st1" points="61.45 113.17 84.08 43.54 38.82 43.54"/>
                    <polygon className="st0" points="38.81 43.54 7.13 43.54 61.43 113.17"/>
                    <path className="st2" d="m7.11 43.56-6.88 21.16c-0.62 1.92 0.05 4.04 1.7 5.24l59.5 43.23z"/>
                    <path className="st1" d="m7.12 43.56h31.71l-13.65-41.93c-0.7-2.15-3.74-2.15-4.47 0z"/>
                    <polygon className="st0" points="84.08 43.54 115.79 43.54 61.45 113.17"/>
                    <path className="st2" d="m115.76 43.56 6.89 21.15c0.62 1.92-0.05 4.04-1.7 5.24l-59.5 43.21z"/>
                    <path className="st1" d="m115.78 43.56h-31.71l13.62-41.93c0.7-2.15 3.74-2.15 4.47 0z"/>
                </g>
            </g>
        </g>
        <style type="text/css">
            {
                ".st0{fill: var(--primary-color-text)} " +
                ".st1{fill: var(--primary-color-text)} " +
                ".st2{fill: var(--primary-color-text)}"
            }
        </style>
    </svg>
)

export default GitLab