import React, { Suspense,Fragment } from 'react';
{/* Inspired from https://prawira.medium.com/react-conditional-import-conditional-css-import-110cc58e0da6 */}

const BootstrapLightBlue = React.lazy(() => import('./bootstrap4-light-blue'));
const BootstrapDarkBlue = React.lazy(() => import('./bootstrap4-dark-blue'));
const LaraDarkTeal = React.lazy(() => import('./lara-dark-teal'));
const LaraLightTeal = React.lazy(() => import('./lara-light-teal'));
const LaraLightIndigo = React.lazy(() => import('./lara-light-indigo'));
const LaraDarkIndigo = React.lazy(() => import('./lara-dark-indigo'));
const LunaPink = React.lazy(() => import('./luna-pink'));

import { ProgressSpinner } from 'primereact/progressspinner';

const ThemeSelector = ({ children }) => {
    const theme = localStorage.getItem('app.theme') || 'lara-light-teal';
    return (
      <Fragment>
        <Suspense fallback={<div className="flex justify-content-start align-items-center min-h-screen"><ProgressSpinner/></div>}>
          {(theme === 'bootstrap4-light-blue') && <BootstrapLightBlue />}
          {(theme === 'bootstrap4-dark-blue') && <BootstrapDarkBlue />}
          {(theme === 'lara-dark-teal') && <LaraDarkTeal />}
          {(theme === 'lara-light-teal') && <LaraLightTeal />}
          {(theme === 'lara-light-indigo') && <LaraLightIndigo />}
          {(theme === 'lara-dark-indigo') && <LaraDarkIndigo />}
          {(theme === 'luna-pink') && <LunaPink />}
        </Suspense>
        {children}
      </Fragment>
    )
  }

  export default ThemeSelector;