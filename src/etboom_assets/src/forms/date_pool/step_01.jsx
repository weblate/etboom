import React,{useState, Fragment} from "react";
import { useForm, Controller } from 'react-hook-form';
import {useNavigate } from 'react-router-dom';
import { InputText } from 'primereact/inputtext';
import { Button } from 'primereact/button';
import { classNames } from 'primereact/utils';
import { Editor } from 'primereact/editor';
import { Dropdown } from 'primereact/dropdown';
import { Calendar } from 'primereact/calendar';
import {useIntl,FormattedMessage} from 'react-intl';


function Step01 ({currentData,updateData}) {
    

    const navigate = useNavigate();
    const navigateTo = (to) => {
        navigate(to, { replace: true });
    };

    const intl = useIntl();
    const today = new Date();
    const month = today.getMonth();
    const year = today.getFullYear();
    const nextMonth = (month === 11) ? 0 : month + 1;
    const nextYear = (nextMonth === 0) ? year + 1 : year;

    let defaultDate = new Date();
    defaultDate.setMonth(nextMonth);
    defaultDate.setFullYear(nextYear);

    let maxDate = new Date();
    maxDate.setMonth(month);
    maxDate.setFullYear(year+1);
    
    const SessionMode = {
        presential: {"presential":""},
        virtual: {"virtual":""},
        hybrid: {"hybrid":""},
        notDefined: {"notDefined":null},
    };

   
    const modes =[
        {name: intl.formatMessage({ id: 'key.date.pool.step01.mode.presential'}), code: SessionMode.presential},
        {name: intl.formatMessage({ id: 'key.date.pool.step01.mode.virtual'}), code: SessionMode.virtual},
        {name: intl.formatMessage({ id: 'key.date.pool.step01.mode.hybrid'}), code: SessionMode.hybrid},
        {name: intl.formatMessage({ id: 'key.date.pool.step01.mode.notdefined'}), code: SessionMode.notDefined}
    ];

    const defaultValues = currentData ? currentData : {
        title: '',
        name: '',
        description: '',
        mode: SessionMode.notDefined,
        replyDate: defaultDate  
    };

    const { control, formState: { errors }, handleSubmit, reset } = useForm({defaultValues});

    const onSubmit = (data) => {
        console.log(data);
        updateData(data);
        navigateTo("/date_pool");
        
        reset();
        
    };

    const getFormErrorMessage = (name) => {
        return errors[name] && <small className="p-error">{errors[name].message}</small>
    };

    return (
            <Fragment>
                <div className="flex flex-grow-0 justify-content-center " >
                    <h1 className="text-center"><FormattedMessage id="key.date.pool.step01.title" defaultMessage="Not translated: key.date.pool.step01.title"/></h1>
                </div>
                <div className="flex flex-grow-0 justify-content-center">

                    <div className="flex flex-grow-0 card-container border-round surface-card m-2 p-2">
                        <form onSubmit={handleSubmit(onSubmit)} className="p-fluid">
                            {/* Title */}
                            <div className="p-field mb-2">
                                <label htmlFor="title"  className={classNames({ 'p-error': errors.title })}>
                                    <FormattedMessage id="key.date.pool.step01.label.title" defaultMessage="Not translated: key.date.pool.step01.label.title"/> *
                                </label>
                                <Controller 
                                    name="title" 
                                    control={control} 
                                    rules={{ required: intl.formatMessage({ id: 'key.date.pool.step01.error.title'}) }} 
                                    render={({ field, fieldState }) => (
                                        <InputText id={field.title} {...field} autoFocus className={classNames({ 'p-invalid': fieldState.invalid })} />
                                )} />
                                {getFormErrorMessage('title')}
                            </div>    
                                  
                            {/* Description */}
                            <div className="p-field mb-2">
                                <label htmlFor="description">
                                    <FormattedMessage id="key.date.pool.step01.label.description" defaultMessage="Not translated: key.date.pool.step01.label.description"/>
                                </label>
                                <Controller 
                                name="description" 
                                control={control} 
                                render={({ field }) => (
                                    <Editor  id={field.description} {...field} onTextChange={(e) => field.onChange(e.htmlValue)} autoResize multiline="true"/>
                                )} />
                            </div>
                 
                            {/* Mode */}
                            <div className="p-field mb-2">    
                                <label htmlFor="mode">
                                    <FormattedMessage id="key.date.pool.step01.label.mode" defaultMessage="Not translated: key.date.pool.step01.label.mode"/>
                                </label>
                                <Controller 
                                    name="mode" 
                                    control={control} 
                                    render={({ field }) => (
                                        <Dropdown id={field.mode} value={field.value} onChange={(e) => field.onChange(e.value)} options={modes} optionLabel="name" optionValue="code"/>
                                )} />
                            </div>

                            {/* reply date */}
                            <div className="p-field">    
                                <label htmlFor="replyDate">
                                    <FormattedMessage id="key.date.pool.step01.label.reply" defaultMessage="Not translated: key.date.pool.step01.label.reply"/>
                                </label>
                                <Controller 
                                    name="replyDate" 
                                    control={control} 
                                    render={({ field }) => (
                                        <Calendar id={field.replyDate} {...field} value={field.value} minDate={today} maxDate={maxDate} className="mb-2"/>
                                )} />
                            </div>
                           
                            <Button type="submit" label={intl.formatMessage({ id: 'key.date.pool.step01.label.submit'})} className="mt-2"/>
                      
                        </form>    
                    </div>

                </div>
                <div className="flex flex-grow-1" />
            </Fragment>
    )

}

export default Step01