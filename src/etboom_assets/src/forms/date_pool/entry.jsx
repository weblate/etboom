import React,{useEffect,useState,useRef,Fragment} from "react";
import {Link, useLocation } from 'react-router-dom';
import Step01 from "./step_01";
import Step02 from "./step_02";
import Step03 from "./step_03";

function Entry () {
    const [step01Data, setStep01Data] = useState(undefined);
    const [step02Data, setStep02Data] = useState(undefined);
    

    useEffect(() => {
        console.info('mount Entry survey to agree on a date');
        console.log("Step01:" + JSON.stringify(step01Data));
        console.log("Step02:" + JSON.stringify(step02Data));
      },[])

    const StepSwitcher = () => {
        const search = useLocation().search;
        const stepNb = new URLSearchParams(search).get('toStepNb');
        if (stepNb) console.log("Go to step n°: " + stepNb);
        if((!step01Data && !stepNb) || stepNb == 1) {
            return <Step01 currentData={step01Data} updateData={setStep01Data}/>
        } else if ((!step02Data && !stepNb) || stepNb == 2) {
            return <Step02 currentData={step02Data} updateData={setStep02Data}/>
        } else if ((step02Data && step01Data && !stepNb) || stepNb == 3) {
            return <Step03 currentData={{
                step01: {...step01Data},
                step02: {...step02Data}
            }}/>
        } else {
            console.log("Step01:" + JSON.stringify(step01Data));
            console.log("Step02:" + JSON.stringify(step02Data));
            return <Link to="/" >Home</Link>
        }

    };
      
      

    return (
            <div className="flex flex-column min-w-full">
                <StepSwitcher/>
            </div>
    )

}

export default Entry