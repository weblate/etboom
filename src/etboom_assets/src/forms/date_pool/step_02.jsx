import React,{useState, Fragment,useRef,useEffect,Suspense} from "react";
import { useForm, Controller } from 'react-hook-form';
import {useNavigate } from 'react-router-dom';
import { Button } from 'primereact/button';
import { Toolbar } from 'primereact/toolbar';
import { Steps } from 'primereact/steps';
import { classNames } from 'primereact/utils';
import { Dialog } from 'primereact/dialog';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { Calendar } from 'primereact/calendar';
import TimeZonesSelect from '../../components/timezones_select';
import {useIntl,FormattedMessage,FormattedDate,FormattedTime} from 'react-intl';


function Step02 ({currentData,updateData}) {
    const [datesArray, setDatesArray] = useState(currentData ? currentData : []);
    const [timesIndex, setTimesIndex] = useState(1);
    const [currentDate,setCurrentDate] = useState(undefined);
    const [datesDialog, setDatesDialog] = useState(false);
    const [deleteDateDialog, setDeleteDateDialog] = useState(false);

    const MAX_NB_PROPOSAL=100;

    useEffect(() => {
        reset();
    }, [datesArray]); 

    useEffect(() => {
        handleDateTimeChange(null);
    }, [timesIndex]); 

    const navigate = useNavigate();
    const navigateTo = (to) => {
        navigate(to, { replace: true });
    };

    const intl = useIntl();
    const today = new Date();
    const month = today.getMonth();
    const year = today.getFullYear();
    const nextMonth = (month === 11) ? 0 : month + 1;
    const nextYear = (nextMonth === 0) ? year + 1 : year;

    let defaultDate = new Date(today.getTime());
    defaultDate.setMonth(nextMonth);
    defaultDate.setFullYear(nextYear);

    let maxDate = new Date(today.getTime());;
    maxDate.setMonth(month);
    maxDate.setFullYear(year+1);
    
    const defaultValues = {
        id: undefined,
        timezone : Intl.DateTimeFormat().resolvedOptions().timeZone,
        selectedDate : today,
        startTime: undefined,
        endTime: undefined,

    };

    const { control, formState: { errors }, handleSubmit, reset,setValue,getValues } = useForm({ defaultValues });

    const onSubmit = (data) => {
        let dataCopy = datesArray.filter(x=>x.id!==data.id);
        data.id = data.id ? data.id : new Date().getTime();
        console.log(data);
        dataCopy.push(data);
        setDatesArray(dataCopy);
        setCurrentDate(undefined);
        //updateData(datesArray);
        //navigateTo("/date_pool?toStepNb=2")   
    };

    const handleBtnNext = () => {
        updateData(datesArray);
        navigateTo("/date_pool")
        reset();
    };

    const handleBtnPrevious = () => {
        updateData(datesArray);
        navigateTo("/date_pool?toStepNb=1")
    };

    const handleDateTimeChange = (event) => {
        const times = getValues(["startTime", "endTime","selectedDate"]);
        const oneDay = 86400000;
        const halfHour = 1800000;
        let startTime = event && event.target.name === 'startTime' ? event.value : times[0];
        let endTime = times[1];
        let selectedDate = event && event.target.name === 'selectedDate' ? event.value : times[2];

        let nStartTime = undefined;
        let nEndTime = undefined;
        let diffTime = undefined;

        if (startTime && endTime) {
            diffTime = endTime.getTime() - startTime.getTime();
        }
        if (diffTime && diffTime<0) {
            diffTime = Math.abs(diffTime);
        }
        if(diffTime && diffTime > oneDay) {
            diffTime = diffTime - oneDay;
        } 
        console.debug("difftime: " + diffTime);

        if (startTime) {
            nStartTime = new Date(selectedDate.getTime());
            nStartTime.setHours(startTime.getHours());
            nStartTime.setMinutes(startTime.getMinutes());
        } else {
            nStartTime = new Date(selectedDate.getTime());
            nStartTime.setHours(8);
            nStartTime.setMinutes(0);
        }
        setValue("startTime",nStartTime,true);

        switch (timesIndex) { 
            case 0 :
                nEndTime = new Date(nStartTime.getTime() + halfHour);
                break;
            case 1:
                nEndTime = new Date(nStartTime.getTime() + halfHour * 2);
                break;
            case 2:
                nEndTime = new Date(nStartTime.getTime() + halfHour * 3);
                break;
            case 3:
                nEndTime = new Date(nStartTime.getTime() + halfHour * 4);
                break;
            default :
                nEndTime = new Date(nStartTime.getTime() + diffTime);
        }

        setValue("endTime",nEndTime,true);

        setValue("selectedDate",selectedDate,true);

    };

    const getFormErrorMessage = (name) => {
        return errors[name] && <small className="p-error">{errors[name].message}</small>
    };

    const predefinedTimes = [
        {
            label: intl.formatMessage({ id: 'key.date.pool.step02.pTimes.label.30', defaultMessage: '0h30'}),
            command: (event) => {
                console.debug("30min");
            }
        },
        {
            label: intl.formatMessage({ id: 'key.date.pool.step02.pTimes.label.60', defaultMessage: '1h00'}),
            command: (event) => {
                console.debug("1h00");
            }
        },
        {
            label: intl.formatMessage({ id: 'key.date.pool.step02.pTimes.label.90', defaultMessage: '1h30'}),
            command: (event) => {
                console.debug("1h30");
            }
        },
        {
            label: intl.formatMessage({ id: 'key.date.pool.step02.pTimes.label.120', defaultMessage: '2h00'}),
            command: (event) => {
                console.debug("2h00");
            }
        },
        {
            label: intl.formatMessage({ id: 'key.date.pool.step02.pTimes.label.other', defaultMessage: 'key.date.pool.step02.pTimes.label.other'}),
            command: (event) => {
                console.debug("autres");
            }
        },
    ]

    const actionBodyTemplate = (rowData) => {
        return (
            <React.Fragment>
                <Button icon="pi pi-pencil" className="p-button-rounded p-button-text mr-2" onClick={() => openDatesDialog(rowData)} />
                <Button icon="pi pi-trash" className="p-button-rounded p-button-text p-button-warning" onClick={() => confirmDeleteDate(rowData)} />
            </React.Fragment>
        );
    }

    const startTimeBodyTemplate = (rowData) => {
        return intl.formatTime(rowData.startTime)
    }

    const endTimeBodyTemplate = (rowData) => {
        return intl.formatTime(rowData.endTime)
    }

    const selectedDateBodyTemplate = (rowData) => {
        return intl.formatDate(rowData.selectedDate)
    }
    const leftToolbarTemplate = () => {
        return <Button type="button" icon="pi pi-bars" className="p-button-rounded p-button-text"  onClick={(e) => openDatesDialog(null)} disabled={datesArray.length>MAX_NB_PROPOSAL}/>
    }

    const hideDeleteDateDialog = () => {
        setDeleteDateDialog(false);
    }

    const removeCurrentDate = () => {
        if(currentDate) {
            let newArrayDates = datesArray.filter(x=>x.id !== currentDate.id);
            setDatesArray(newArrayDates);
        } else {
            console.warn("Try delete an empty row !");
        }
        setDeleteDateDialog(false);
        setCurrentDate(undefined);
    }

    const confirmDeleteDate = (raw) => {
        setCurrentDate(raw);
        setDeleteDateDialog(true);
    }

    const deleteDateDialogFooter = (
        <React.Fragment>
            <Button label={intl.formatMessage({ id: 'key.date.pool.step02.dialog.delete.btn.cancel',defaultMessage:'key.date.pool.step02.dialog.delete.btn.cancel'})} icon="pi pi-times" className="p-button-outlined" onClick={hideDeleteDateDialog} />
            <Button label={intl.formatMessage({ id: 'key.date.pool.step02.dialog.delete.btn.delete',defaultMessage:'key.date.pool.step02.dialog.delete.btn.delete'})}  icon="pi pi-check" className="p-button-outlined" onClick={removeCurrentDate} />
        </React.Fragment>
    );

    const hideDatesDialog = () => {
        setDatesDialog(false);
        setCurrentDate(undefined);
    }

    const openDatesDialog = (raw) => {
        setCurrentDate(raw);
        if (raw) {
            setValue("id",raw.id,false);
            setValue("selectedDate",raw.selectedDate,true);
            setValue("startTime",raw.startTime,true);
            setValue("endTime",raw.endTime,true);
        }
        setDatesDialog(true);
    }

    const datesDialogFooter = (
        <React.Fragment>
            <Button label={intl.formatMessage({ id: 'key.date.pool.step02.dialog.date.btn.close',defaultMessage:'key.date.pool.step02.dialog.date.btn.close'})} icon="pi pi-times" className="p-button-outlined" onClick={hideDatesDialog} />
        </React.Fragment>
    );

    return (
            <Fragment>
                {/* Dialog to confirm delete of row in table */}
                <Dialog visible={deleteDateDialog} style={{ width: '450px' }} header={intl.formatMessage({ id: 'key.date.pool.step02.dialog.delete.title',defaultMessage:'key.date.pool.step02.dialog.delete.title'})} modal footer={deleteDateDialogFooter} onHide={hideDeleteDateDialog}>
                    <div className="confirmation-content">
                        <i className="pi pi-exclamation-triangle p-mr-3" style={{ fontSize: '2rem'}} />&nbsp;
                        <span><FormattedMessage id="key.date.pool.step02.dialog.delete.msg" defaultMessage="Not translated: key.date.pool.step02.dialog.delete.msg"/></span>
                        <FormattedDate value={currentDate && currentDate.selectedDate}/>&nbsp;
                        <FormattedTime value={currentDate && currentDate.startTime}/>&nbsp;
                        <FormattedTime value={currentDate && currentDate.endTime}/>&nbsp;
                    </div>
                </Dialog>

                {/* Form as Dialog to create meeting proposal */}
                <Dialog visible={datesDialog} header={intl.formatMessage({ id: 'key.date.pool.step02.dialog.date.title',defaultMessage:'key.date.pool.step02.dialog.date.title'})} footer={datesDialogFooter} modal  onHide={hideDatesDialog}>
                    <form onSubmit={handleSubmit(onSubmit)} >
                        <div className="formgrid grid">
                            <div className="field col">    
                                <label htmlFor="selectedDate" className={classNames({ 'p-error': errors.selectedDate })}>
                                    <FormattedMessage id="key.date.pool.step02.selected.date" defaultMessage="Not translated: key.date.pool.step02.selected.date"/>*
                                </label>
                                <Controller 
                                    name="selectedDate" 
                                    control={control} 
                                    rules={{ required: intl.formatMessage({ id: 'key.date.pool.step02.error.date',defaultMessage:'key.date.pool.step02.error.date'}) }} 
                                    render={({ field, fieldState }) => (
                                        <Calendar id={field.selectedDate} {...field} value={field.value} minDate={today} maxDate={maxDate} className={classNames({ 'p-invalid': fieldState.invalid }) + " mb-2 w-full"} onChange={(e)=>handleDateTimeChange(e)}/>
                                )} />
                                {getFormErrorMessage('selectedDate')}
                            </div>
                            <div className="field col">    
                                <label htmlFor="timezone" className="mr-2">
                                    <FormattedMessage id="key.date.pool.step02.label.timeZone" defaultMessage="Not translated: key.date.pool.step02.label.timeZone"/>
                                </label>
                                <Controller 
                                    name="timezone" 
                                    control={control} 
                                    render={({field:{ref, ...rest} }) => (
                                        <TimeZonesSelect {...rest}/>
                                )} />
                            </div>
                        </div> 
                        <div className="field">    
                            <Steps model={predefinedTimes} activeIndex={timesIndex} onSelect={(e) => setTimesIndex(e.index)} readOnly={false} onChange={(e)=>handleDateTimeChange(e)}/>  
                        </div>
                        <div className="formgrid grid">
                            <div className="field col"> 
                                <label htmlFor="startTime" className={classNames({ 'p-error': errors.startTime })}>
                                    <FormattedMessage id="key.date.pool.step02.selected.startTime" defaultMessage="Not translated: key.date.pool.step02.selected.startTime"/>*
                                </label>
                                <Controller 
                                    name="startTime" 
                                    control={control} 
                                    rules={{ required: intl.formatMessage({ id: 'key.date.pool.step02.error.startTime',defaultMessage:'key.date.pool.step02.error.startTime'}) }} 
                                    render={({ field, fieldState }) => (
                                        <Calendar id={field.startTime} {...field} value={field.value} className={classNames({ 'p-invalid': fieldState.invalid }) + " mb-2 w-full"}  timeOnly onChange={(e)=>handleDateTimeChange(e)}/>
                                )} />
                                {getFormErrorMessage('startTime')}
                            </div>
                            <div className="field col"> 
                                <label htmlFor="endTime" className={classNames({ 'p-error': errors.endTime})}>
                                    <FormattedMessage id="key.date.pool.step02.selected.endTime" defaultMessage="Not translated: key.date.pool.step02.selected.endTime"/>*
                                </label>
                                <Controller 
                                    name="endTime" 
                                    control={control} 
                                    rules={{ required: intl.formatMessage({ id: 'key.date.pool.step02.error.endTime',defaultMessage:'key.date.pool.step02.error.endTime'}) }} 
                                    render={({ field, fieldState }) => (
                                        <Calendar id={field.endTime} {...field} value={field.value} className={classNames({ 'p-invalid': fieldState.invalid }) + " mb-2 w-full"}  timeOnly {...(timesIndex<4 && {disabled:true})}/>
                                )} />
                                {getFormErrorMessage('endTime')}
                            </div>   
                        </div>
                        <div className="field">    
                            <Button type="submit" icon="pi pi-plus" className="p-button-rounded" disabled={datesArray.length>MAX_NB_PROPOSAL}/>
                        </div>
                    </form>  
                </Dialog>
                
                <div className="flex flex-grow-0 justify-content-center " >
                    <h1 className="text-center"><FormattedMessage id="key.date.pool.step02.title" defaultMessage="Not translated: key.date.pool.step02.title"/></h1>
                </div>
                {/* Table vue and results */}
                <div className="flex flex-grow-0 justify-content-center">
                    <div className="flex flex-column justify-content-center card-container border-round surface-card m-2 p-2">
                        <div className="flex">
                            <Toolbar className="mb-2 min-w-full" left={leftToolbarTemplate}/>
                        </div> 
                        <div className="flex">
                            <DataTable value={datesArray}
                                dataKey="id"
                                stripedRows
                                responsiveLayout="scroll"
                                size="small">
                                    <Column field="selectedDate" body={selectedDateBodyTemplate} header={intl.formatMessage({ id: 'key.date.pool.step02.table.header.c1',defaultMessage: 'key.date.pool.step02.table.header.c1'})} ></Column>
                                    <Column field="startTime" body={startTimeBodyTemplate} header={intl.formatMessage({ id: 'key.date.pool.step02.table.header.c2',defaultMessage: 'key.date.pool.step02.table.header.c2'})}></Column>
                                    <Column field="endTime" body={endTimeBodyTemplate} header={intl.formatMessage({ id: 'key.date.pool.step02.table.header.c3',defaultMessage: 'key.date.pool.step02.table.header.c3'})}></Column>
                                    <Column body={actionBodyTemplate} header={intl.formatMessage({ id: 'key.date.pool.step02.table.header.c4',defaultMessage: 'key.date.pool.step02.table.header.c4'})} exportable={false} ></Column>
                            </DataTable>
                        </div>
                        <div className="formgrid grid">
                            <div className="field col"> 
                                <Button type="button" label={intl.formatMessage({ id: 'key.date.pool.step02.label.button',defaultMessage: 'key.date.pool.step02.label.button'})} className="mt-2 min-w-full" onClick={(e)=>handleBtnPrevious()}/>
                            </div>
                            <div className="field col"> 
                                <Button type="button" label={intl.formatMessage({ id: 'key.date.pool.step02.label.submit',defaultMessage: 'key.date.pool.step02.label.submit'})} className="mt-2 min-w-full" onClick={(e)=>handleBtnNext()} disabled={!datesArray || datesArray.length===0}/>
                            </div>
                        </div>  
                    </div>
                </div>
                <div className="flex flex-grow-1" />
            </Fragment>
    )

}

export default Step02