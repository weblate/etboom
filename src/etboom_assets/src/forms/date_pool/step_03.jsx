import React,{useState, Fragment,useRef,useEffect,Suspense} from "react";
import {useIntl,FormattedMessage,FormattedDate,FormattedTime} from 'react-intl';
import { Panel } from 'primereact/panel';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { Ripple } from 'primereact/ripple';


function Step03 ({currentData}) {
    const [dataTable, setDataTable] = useState([]);

    useEffect(() => {
        console.log("data: " + JSON.stringify(currentData));
        let data = [];
        data.push(
            {
                name: intl.formatMessage({ id: 'key.date.pool.step03.field.title'}),
                value: currentData.step01.title
            }
        );
        data.push(
            {
                name: intl.formatMessage({ id: 'key.date.pool.step03.field.description'}),
                value: currentData.step01.description
            }
        );
        data.push(
            {
                name: intl.formatMessage({ id: 'key.date.pool.step03.field.reply.date'}),
                value: intl.formatDate(currentData.step01.replyDate)
            }
        );
        data.push(
            {
                name: intl.formatMessage({ id: 'key.date.pool.step03.field.mode'}),
                value: currentData.step01.mode.toString()
            }
        );

        setDataTable([...data]);
    }, []); 

    const intl = useIntl();

    const nameBodyTemplate = (rowData) => {
        return <div className="font-italic font-semibold">{rowData.name}</div>;
    }

    const isDateEqual = (date1,date2) => {
        return true;
    }
    

    return (
        <div className="flex flex-grow-0 justify-content-center ml-4 mr-4">
            <Panel header={intl.formatMessage({ id: 'key.date.pool.step03.title'})}>
                <DataTable value={dataTable} showGridlines responsiveLayout="scroll">
                    <Column field="name" header={intl.formatMessage({ id: 'key.date.pool.step03.table.C1'})} body={nameBodyTemplate} className="bg-primary"/>
                    <Column field="value" header={intl.formatMessage({ id: 'key.date.pool.step03.table.C2'})} className="bg-primary"/>
                </DataTable>
            </Panel>

        </div>
    )

}

export default Step03