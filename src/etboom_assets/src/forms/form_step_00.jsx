import React,{Fragment} from "react";
import { Card } from 'primereact/card';

import {FormattedMessage,FormattedNumber,FormattedDate,FormattedTime,} from 'react-intl';

function FormStep00 () {
    
    const header = (
        <div>header</div>
    );

    const footer = (
        <div>footer</div>
    );

    return (
        <Fragment>
            <Card title="Advanced Card" subTitle="Subtitle" style={{ width: '25em' }} footer={footer} header={header}>
                <p className="p-m-0" style={{lineHeight: '1.5'}}></p>
            </Card>

        </Fragment>
    );
}

export default FormStep00