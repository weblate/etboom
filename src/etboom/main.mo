actor {

    public type SessionModeType = { 
        #presential:Text; 
        #virtual:Text;
        #hybrid:Text;
        #notDefined;
    };

    public func greet(name : Text) : async Text {
        return "Hello, " # name # "!";
    };

    public func getAllSessionMode() : async [SessionModeType] {
        return [#presential("Free Text as location"),#virtual("Free Text as Jami, Element,..."),#hybrid("Free Text ..."),#notDefined];
    };

    public func testSetSessionMode(mode : SessionModeType) : async SessionModeType {
        var newMode :  SessionModeType = #notDefined;
        newMode := mode;
        return newMode;
    };
};
