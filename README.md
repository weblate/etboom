# etboom

| :warning: WARNING                                                                                                                           |
|:--------------------------------------------------------------------------------------------------------------------------------------------|
| This project is in the exploratory phase, it is not in a usable state.                                                                      |

Inspired by Doodle but open source and without the tracking. Use for appointment scheduling.

## Build, Dev

### Prerequisites

* [dFinity SDK](https://smartcontracts.org) or 
    * `sh -ci "$(curl -fsSL https://smartcontracts.org/install.sh)"`
* npm

### Clone repo. 

`git clone https://gitlab.com/kurdy/etboom.git`

`cd etboom`

### Install dependencies

`npm install`

### Starts the local replica and a web server 

`dfx start &`

### Deploy

`dfx deploy`

### Front-end dev

`npm start`

## Memo: links may change according your configuration.

http://127.0.0.1:8000/?canisterId=ryjl3-tyaaa-aaaaa-aaaba-cai#/

http://127.0.0.1:8000/?canisterId=r7inp-6aaaa-aaaaa-aaabq-cai

http://localhost:8085/#/

## Essential references and resources used by the project 

* Internet Computer [DFINITY](https://dfinity.org)
* PrimeReact [Prime](https://www.primefaces.org)
* Icons [game-icons.net](https://game-icons.net)
* SVGR [PlayGround](https://react-svgr.com/playground/)
* Icons [uxwing](https://uxwing.com)
* Others 😍 [Manjaro](https://manjaro.org), [FSF](https://www.fsf.org), [Code OSS](https://github.com/Microsoft/vscode), [React](https://reactjs.org), [Node.js®](https://nodejs.org), [Docker](https://www.docker.com), [npm](https://www.npmjs.com/), [WebPack](https://webpack.js.org), [GitLab](https://gitlab.com)